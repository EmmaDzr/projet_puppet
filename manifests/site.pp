node default {


    apache::vhost { 'cocky_chandrasekhar.dgr.ovh':
        port    => '80',
        docroot => '/data/www/',
        redirect_status => 'permanent',
        redirect_dest   => 'https://92.222.115.21/graphs'
    }

    class { '::mysql::server':
        root_password           => 'strongpassword',
        restart                 => true,
    }

    class { 'php':
        ensure       => 'present',
        manage_repos => false,
        fpm          => true,
        dev          => false,
        composer     => false,
        pear         => true,
        phpunit      => false,
        fpm_pools    => {},
    }

        Firewall {
            require => undef,
        }

    # Default firewall rules
        firewall { '000 accept ssh':
            proto  => 'ssh',
            action => 'accept',
        }
        firewall { '001 accept http':
            proto  => 'http',
            action => 'accept',
        }
        firewall { '002 accept https':
            proto  => 'https',
            action => 'accept',
        }
        firewall { '003 accept related established rules':
            proto  => 'all',
            state  => ['RELATED', 'ESTABLISHED'],
            action => 'accept',
        }

    class { 'grafana':
      cfg => {
        app_mode => 'production',
        server   => {
          http_port     => 8080,
        },
        database => {
          type          => 'mysql',
          host          => '127.0.0.1:3306',
          name          => 'grafana',
          user          => 'root',
          password      => '',
        },
        users    => {
          allow_sign_up => false,
        },
      },
    }

    class { 'influxdb' :}

    class { 'telegraf':
        hostname => $facts['s006858'],
        outputs  => {
            'influxdb' => [
                {
                'urls'     => [ "https://127.0.0.1" ],
                'database' => 'telegraf',
                'username' => 'telegraf',
                'password' => '',
                }
            ]
        },
    }

    class { 'ghost':
        include_nodejs => true,
    }   -> ghost::blog { 'https://92.222.115.21/graphs': }


    class { 'docker':
        tcp_bind        => ['tcp://127.0.0.1:2375'],
        socket_bind     => 'unix:///var/run/docker.sock',
        ip_forward      => true,
        iptables        => true,
        ip_masq         => true,
    }
}
